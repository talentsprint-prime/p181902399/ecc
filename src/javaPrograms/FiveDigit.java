package javaPrograms;

import java.util.Scanner;

public class FiveDigit {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("enter a number:");
		int num = scan.nextInt();
		boolean result = FiveMethod(num);
		System.out.println(result);

	}

	public static boolean FiveMethod(int num) {
		if ((num > 10000) && (num < 999999))
			return true;
		else
			return false;
	}

}

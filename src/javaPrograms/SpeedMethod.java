package javaPrograms;

import java.util.Scanner;

public class SpeedMethod {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int milesPerSec, kmPerHr, milesPerHr;
		double result1, result2, result3;
		System.out.println("enter the distance:");
		int distance = scan.nextInt();
		milesPerSec = scan.nextInt();
		kmPerHr = scan.nextInt();
		milesPerHr = scan.nextInt();
		System.out.println(milesPerSec + "h" + kmPerHr + "m" + milesPerHr + "s");
		int totalTimeInSec = timeSec(milesPerSec, kmPerHr, milesPerHr);
		result1 = speedMs(distance, totalTimeInSec);
		System.out.println("speed in m/s" + result1);
		result2 = speedKmHr(distance, totalTimeInSec);
		System.out.println("speed in km/hr" + result2);
		result3 = speedMihr(distance, totalTimeInSec);
		System.out.println("speed in mi/hr" + result3);
	}

	public static int timeSec(int p1, int p2, int p3) {
		int time = (p1 * 60 * 60) + (p2 * 60) + (p3);
		return time;
	}

	public static double speedMs(double distance, int time) {

		double result = distance / time;
		return result;
	}

	public static double speedKmHr(double distance, int time) {

		double result = (distance / time) * (18 / 5);
		return result;
	}

	public static double speedMihr(double distance, int time) {

		double result = (distance / time) * (3600 / 1609);
		return result;
	}

}

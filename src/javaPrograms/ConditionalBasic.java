package javaPrograms;

public class ConditionalBasic {

	public static void main(String[] args) {

		int age = 91;
		
		if (age > 90) {
			System.out.println("your age is above 90");
		}

		else if (age > 50 && age < 90) {
			System.out.println("your age is between 50 and 90");
		}

		else {
			System.out.println("your age is less than 50");
		}

	}

}

package javaPrograms;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("enter a year:");
		int year = scan.nextInt();

		boolean result = leap(year);
		System.out.println(result);

		if (result)
			System.out.println(year + "is a leap year");
		else
			System.out.println(year + "is not a leap year");
	}

	public static boolean leap(int year) {

		if ((year % 400 == 0)) {
			return true;
		} else if ((year % 4 == 0) && (year % 100 != 0))
			return true;
		else
			return false;
	}
}

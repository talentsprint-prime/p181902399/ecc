package javaPrograms;

import java.util.Scanner;

public class MaxNumber {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("enter first number:");
		int s1 = scan.nextInt();
		System.out.println("enter second number:");
		int s2 = scan.nextInt();
		System.out.println("enter third number:");
		int s3 = scan.nextInt();

		int result = max(s1, s2, s3);
		System.out.println(result);
	}

	public static int max(int s1, int s2, int s3) {
		int max = s1;
		if (s2 > max) {
			return max = s2;
		} else if (s3 > max) {
			return max = s3;
		} else
			return max;
	}
}

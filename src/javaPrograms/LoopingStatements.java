package javaPrograms;

public class LoopingStatements {

	public static void main(String[] args) {

		int num = 15;
		while (num > 0) {
			System.out.println("countdown :" + num);
			num -= 1;
		}

	}

}

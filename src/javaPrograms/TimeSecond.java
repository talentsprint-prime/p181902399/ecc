package javaPrograms;

import java.util.Scanner;

public class TimeSecond {

	public static void main(String[] args) {

		int hour, minute, sec, sta;
		Scanner scan = new Scanner(System.in);
		System.out.println("enter number of hours,minutes,sec,sta:");
		hour = scan.nextInt();
		minute = scan.nextInt();
		sec = scan.nextInt();
		sta = scan.nextInt();
		int newsec = timeToSec(hour, minute, sec);
		int totalsec = newsec + sta;
		secToTime(totalsec);
	}

	public static int timeToSec(int hour, int minutes, int sec) {
		return (hour * 60 * 60) + (minutes * 60) + sec;
	}

	public static void secToTime(int totalsec) {
		int newhour = totalsec / 3600;
		int remsec = totalsec % 3600;
		int newminutes = remsec / 60;
		int newsec = remsec % 60;
		System.out.println("The new time is" + newhour + ":" + newminutes + ":" + newsec + ":");

	}

}

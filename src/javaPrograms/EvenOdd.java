package javaPrograms;

import java.util.Scanner;

public class EvenOdd {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("enter a number:");
		int num = scan.nextInt();

		boolean result = even(num);
		System.out.println(result);

		if (result)
			System.out.println(num + "is even");
		else
			System.out.println(num + "is odd");

	}

	public static boolean even(int num) {
		if (num % 2 == 0)
			return true;

		else
			return false;

	}
}

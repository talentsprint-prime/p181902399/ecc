package javaPrograms;

import java.util.Scanner;

public class Circle {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		double r;
		System.out.println("enter radius:");
		r = scan.nextDouble();

		double area1 = area(r);
		double perimeter1 = perimeter(r);

		System.out.println("area is" + area1);
		System.out.println("perimeter is" + perimeter1);
	}

	public static double area(double r) {
		double pi = 3.14;
		double c = pi * r * r;
		return c;

	}

	public static double perimeter(double r) {
		double pi = 3.14;
		double c = 2 * pi * r;
		return c;
	}

}

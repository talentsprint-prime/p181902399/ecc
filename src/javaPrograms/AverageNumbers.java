package javaPrograms;

import java.util.Scanner;

public class AverageNumbers {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("enter three numbers");
		int num1 = scan.nextInt();
		int num2 = scan.nextInt();
		int num3 = scan.nextInt();

		double result = average(num1, num2, num3);
		System.out.println("the average is: " + result);

	}

	public static double average(int num1, int num2, int num3) {

		double result = (num1 + num2 + num3) / 3.0;
		return result;

	}

}

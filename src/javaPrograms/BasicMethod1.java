package javaPrograms;

import java.util.Scanner;

public class BasicMethod1 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("enter num1:");
		int num1 = scan.nextInt();

		System.out.println("enter num2:");
		int num2 = scan.nextInt();

		int result1 = sum(num1, num2);
		int result2 = sub(num1, num2);
		int result3 = mul(num1, num2);
		int result4 = div(num1, num2);

		System.out.println("result is" + result1);
		System.out.println("result is" + result2);
		System.out.println("result is" + result3);
		System.out.println("result is" + result4);
	}

	public static int sum(int a, int b) {
		int c = (a + b);
		return c;
	}

	public static int sub(int a, int b) {
		int c = (a - b);
		return c;
	}

	public static int mul(int a, int b) {
		int c = (a * b);
		return c;
	}

	public static int div(int a, int b) {
		int c = (a / b);
		return c;
	}
}

package javaPrograms;

import java.util.Scanner;

public class Grade {

	public static void main(String[] args) {

		double maths;
		int physics, chemistry, totalMarks;
		Scanner scan = new Scanner(System.in);

		System.out.println("enter maths marks:");
		maths = scan.nextDouble();
		System.out.println("enter physics marks:");
		physics = scan.nextInt();
		System.out.println("enter chemistry marks:");
		chemistry = scan.nextInt();
		System.out.println("enter your total marks:");
		totalMarks = scan.nextInt();

		String result = averageOfSubjects(maths, physics, chemistry, totalMarks);
		System.out.println(result);

	}

	public static String averageOfSubjects(double maths, int physics, int chemistry, int totalMarks) {

		String result;
		double percentage = ((maths + physics + chemistry) * 100) / totalMarks;

		if (percentage >= 90) {
			result = "congrats! you have got score A";
			return result;

		} else if ((percentage >= 70) && (percentage < 90)) {
			result = "congrats! you have got score B";
			return result;

		} else if ((percentage >= 50) && (percentage < 70)) {
			result = "congrats! you have got score c";
			return result;

		} else
			result = "you failed the test";
		return result;

	}

}

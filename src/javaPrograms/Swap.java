package javaPrograms;

import java.util.Scanner;

public class Swap {

	public static void main(String[] args) {

		int t;
		int a = 10, b = 5;
		t = a;
		a = b;
		b = t;
		System.out.println("a is " + a);
		System.out.println("b is " + b);

	}
}

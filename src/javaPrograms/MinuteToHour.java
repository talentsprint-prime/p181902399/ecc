package javaPrograms;

import java.util.Scanner;

public class MinuteToHour {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("enter no of minutes:");

		int minutes = scan.nextInt();
		int years = minutes / (365 * 24 * 60);
		int left = minutes % (365 * 24 * 60);
		int days = left / 1440;
		int leftm = left % 1440;
		int hours = leftm / 60;
		int min = leftm % 60;

		System.out.println("no of years is: " + years);
		System.out.println("no of days is: :" + days);
		System.out.println("no of hours is: " + hours);
		System.out.println("no of minutes is :" + min);
	}
}

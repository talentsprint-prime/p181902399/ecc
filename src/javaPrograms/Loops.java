package javaPrograms;

public class Loops {

	public static void main(String[] args) {

		int age = 20;

		switch (age) {
		case 90:
			System.out.println("congrats! your age is 90");
			break;
		case 80:
			System.out.println("congrats! your age is 80");
			break;
		case 70:
			System.out.println("congrats! your age is 70");
			break;
		case 60:
			System.out.println("congrats! your age is 60");
			break;
		case 50:
			System.out.println("congrats! your age is 50");
			break;

		default:
			System.out.println("your age is pretty good");
			break;
		}

	}

}

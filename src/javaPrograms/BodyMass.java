package javaPrograms;

import java.util.Scanner;

public class BodyMass {

	public static void main(String[] args) {

		double weight, height;
		Scanner scan = new Scanner(System.in);

		System.out.println("enter your weight:");
		weight = scan.nextDouble();

		System.out.println("enter your height:");
		height = scan.nextDouble();

		double bmi = Bmi(weight, height);
		System.out.println("bmi is:" + bmi);
	}

	public static double Bmi(double weight, double height) {

		double bmi = weight / (height * height);
		return bmi;

	}

}

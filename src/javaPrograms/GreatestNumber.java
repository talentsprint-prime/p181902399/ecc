package javaPrograms;

import java.util.Scanner;

public class GreatestNumber {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("enter first number:");
		int s1 = scan.nextInt();

		System.out.println("enter second number:");
		int s2 = scan.nextInt();

		System.out.println("enter third number:");
		int s3 = scan.nextInt();

		int result = max(s1, s2, s3);
		System.out.println(result);
	}

	public static int max(int n1, int n2, int n3) {

		if ((n1 > n2) && (n1 > n3))
			return n1;
		else if (n2 > n3)
			return n2;
		else
			return n3;
	}

}

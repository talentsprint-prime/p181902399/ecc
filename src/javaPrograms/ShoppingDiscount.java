package javaPrograms;

import java.util.Scanner;

public class ShoppingDiscount {

	public static void main(String[] args) {

		double sellPrice = 0;
		Scanner scan = new Scanner(System.in);
		System.out.println("enter the price of items:");
		double costPrice = scan.nextDouble();
		display(costPrice, sellPrice);
	}

	public static void display(double costPrice, double sellPrice) {
		if ((costPrice > 0) && (costPrice < 10000)) {
			System.out.println("you get the discount of 10%");
			double result = costPrice * 10 / 100;
			sellPrice = costPrice - result;
			System.out.println("the selling prise is" + sellPrice);
		} else {
			if ((costPrice > 10000) && (costPrice <= 20000)) {
				System.out.println("you get the discount of 20%");
				double result = costPrice * 20 / 100;
				sellPrice = costPrice - result;
				System.out.println("the selling prise is" + sellPrice);

			} else {

				System.out.println("you get the discount of 25%");
				double result = costPrice * 25 / 100;
				sellPrice = costPrice - result;
				System.out.println("the selling prise is" + sellPrice);
			}
		}
	}
}
